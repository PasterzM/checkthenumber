﻿using UnityEngine.UI;
using UnityEngine;
using System.Collections.Generic;
//using UnityEngine.Advertisements;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
public class GameManager : MonoBehaviour {
    enum GameState {
        GAME,
        GAMEOVER,
        MENU,
        SETTINGS
    };

    public Text mNumStringRep;
    public int mNumIntRep;
    public int mNumIntPattern;  // cyfra która jest brana pod uwagę przy sprawdzeniach
    int mHighScore;

    public float mClickedTime;
    public float mElapseTimeBeetwenTouches;
    float mDt;

    bool isClicked = false;

    GameState mGameState;

    [Space(10)]
    public Dictionary<string, GameObject> mPanels;
    public GameObject mFatherOfPanels;

    //public GameObject mBackgroundPanel;
    //public GameObject mMenuPanel;
    //public GameObject mGamePanel;
    //public GameObject mSettingsPanel;
    //public GameObject mAboutGamePanel;
    [Space(10)]
    public Sprite mSprBackground;
    public Sprite mSprFailed;
    [Space(10)]
    public Text mPaternNumerInputInSettingsText;
    public Text mPaternInGameText;
    public Text mTimeToClickInSettingsText;
    public Text mTimeToClickIntGameText;

    [Space(10)]
    public AudioClip mFailedSoundEffect;
    public AudioClip mReceivePointSoundEffect;
    AudioSource mAudioSource;

    [Space(10)]
    public LanguagesDataBase mLanguges;
    public Text mInstructionLanguageText;
    public Text mHighScoreText;

    void Start() {
        InitGame();
        Screen.autorotateToPortrait = false;
        Screen.autorotateToPortraitUpsideDown = false;
        Screen.autorotateToLandscapeLeft = false;
        Screen.autorotateToLandscapeRight = false;
        mGameState = GameState.MENU;
        mAudioSource = GetComponent<AudioSource>();
        mHighScore = 0;
        LoadHighScore();
        mPanels = new Dictionary<string, GameObject>();
        for (int i = 0; i < mFatherOfPanels.transform.childCount; ++i) {
            GameObject item = mFatherOfPanels.transform.GetChild(i).gameObject;
            mPanels.Add(item.name, item);
            if (item.name != "MenuPanel")
                item.SetActive(false);
            else
                item.SetActive(true);
        }
    }

    void InitGame() {
        mNumIntRep = 1;
        mDt = 0f;
        mTimeToClickIntGameText.text = (mClickedTime - mDt).ToString("0.00") + " sec";
        mNumStringRep.text = mNumIntRep.ToString();
        mGameState = GameState.GAME;
        mElapseTimeBeetwenTouches = .25f;
        isClicked = false;
        mPaternInGameText.text = mNumIntPattern.ToString();
        ResetInputTouches();
    }

    // Update is called once per frame
    void Update() {
        switch (mGameState) {
            case GameState.GAME:
                Game();
                break;
            case GameState.GAMEOVER:
                GameOver();
                break;
            case GameState.MENU:
                //
                break;
        }

    }

    private void GameOver() {
        if (!isClicked) {
            if (Input.touchCount > 0) {
                SetPanel(mPanels["MenuPanel"]);
            }
        } else {
            Wait();
        }
    }

    private void Game() {
        if (!isClicked) {
            TryGuest();
        } else {
            Wait();
        }

    }

    private void Wait() {
        if (mDt >= mElapseTimeBeetwenTouches) {
            mDt = 0f;
            isClicked = false;
            ResetInputTouches();
        } else {
            mDt += Time.deltaTime;
        }
    }

    void ResetInputTouches() {
        Touch[] touches = Input.touches;
        for (int i = 0; i < touches.Length; ++i) {
            touches[i].phase = TouchPhase.Ended;
        }
    }

    private void TryGuest() {
        if (Input.touchCount > 0 && Input.touches[0].phase != TouchPhase.Ended) {
            isClicked = true;
            if (!CheckNumber()) {
                //Debug.Log("Punkt za: " + mNumStringRep.text);
                SetNextNumber();
            } else {
                SetFailed();
            }
        } else if (mDt >= mClickedTime) {
            if (CheckNumber()) {
                SetNextNumber();
            } else {
                SetFailed();
                mTimeToClickIntGameText.text = "0.00 sec";
            }
        } else {
            mDt += Time.deltaTime;
            mTimeToClickIntGameText.text = (mClickedTime - mDt).ToString("0.00") + " sec";
        }
    }

    bool CheckNumber() {
        if (mNumIntRep % mNumIntPattern == 0 || mNumStringRep.text.Contains(mNumIntPattern.ToString())) {
            return true;
        }
        return false;
    }

    void SetNextNumber() {
        mDt = 0f;

        mAudioSource.PlayOneShot(mReceivePointSoundEffect);
        mNumStringRep.text = (++mNumIntRep).ToString();
        ResetInputTouches();
        //TODO: dodaj punkt, albo uzyj "mNumIntRep"
    }

    void SetFailed() { //porazka
        //TODO: dzwiek przegranej
        mAudioSource.PlayOneShot(mFailedSoundEffect, 1f);
        mGameState = GameState.GAMEOVER;
        mFatherOfPanels.GetComponent<Image>().sprite = mSprFailed;
        if (mNumIntRep > mHighScore) {
            SaveHighScore();
        }
        ResetInputTouches();
        //wyswielt porazke
    }
    public void SetPanel(GameObject panel) {
        foreach (var item in mPanels) {
            if (item.Key == panel.name) {
                item.Value.SetActive(true);
            } else {
                item.Value.SetActive(false);
            }
        }

        if (panel.name == "GamePanel") {
            InitGame();
        } else {
            mGameState = GameState.MENU;
            mHighScoreText.text = "TopScore: " + mHighScore.ToString();
        }

        mFatherOfPanels.GetComponent<Image>().sprite = mSprBackground;
    }

    #region SAVE_LOAD_SCORES
    void SaveHighScore() {
        if (mHighScore < mNumIntRep) {
            mHighScore = mNumIntRep;
            mHighScoreText.text = "TopScore: " + mHighScore.ToString();
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenWrite(Application.persistentDataPath + "/playerInfo.dat");
            bf.Serialize(file, mNumIntRep);
            file.Close();
        }
    }

    void LoadHighScore() {
        string path = Application.persistentDataPath + "/playerInfo.dat";
        if (File.Exists(path)) {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.OpenRead(path);
            if (file.Length > 0)
                mHighScore = (int)bf.Deserialize(file);
            mHighScoreText.text = "TopScore: " + mHighScore.ToString();
            file.Close();
        } else {
            FileStream file = File.Create(Application.persistentDataPath + "/playerInfo.dat");
            mHighScore = 0;
            mHighScoreText.text = "TopScore: " + mHighScore.ToString();
            file.Close();
        }
    }

    #endregion

    #region INCREMENT_DECREMENT

    public void IncrementPatern() {
        if (mNumIntPattern + 1 > 9)
            mNumIntPattern = 2;
        else
            ++mNumIntPattern;
        mPaternNumerInputInSettingsText.text = mNumIntPattern.ToString();
    }
    public void DecrementPatern() {
        if (mNumIntPattern - 1 < 2)
            mNumIntPattern = 9;
        else
            --mNumIntPattern;
        mPaternNumerInputInSettingsText.text = mNumIntPattern.ToString();
    }

    public void IncrementClickedTime() {
        if (mClickedTime + .25f > 10f) {
            mClickedTime = .5f;
        } else {
            mClickedTime += .25f;
        }
        mTimeToClickInSettingsText.text = mClickedTime.ToString();
    }

    public void DecrementClickedTime() {
        if (mClickedTime - .25f < .5f)
            mClickedTime = 10f;
        else {
            mClickedTime -= .25f;
        }
        mTimeToClickInSettingsText.text = mClickedTime.ToString();
    }

    #endregion

    #region GAME_INSTRUCTION

    public void SetLanguage(Dropdown language) {
        mInstructionLanguageText.text = mLanguges.mPrototypes[language.value].mDescription;

    }
    #endregion

}