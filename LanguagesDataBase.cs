﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Languages", menuName = "ScriptableObjects/Languages")]
public class LanguagesDataBase : ScriptableObject {

    [Serializable]
    public class Language {
        public string mName;
        [TextArea(10, 10)] public string mDescription;
    }

    public Language[] mPrototypes;
}
